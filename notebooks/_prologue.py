import sys
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

os.system('pip install zstandard 2>&1 > /dev/null')
sys.path.append(os.getcwd() + '/..')

from csb import CsbIndex
from analysis import *

sns.set_context({ 'figure.figsize': (15, 12) })
sns.set_theme(style="whitegrid")

def load_features_xy(glob, flavor=None, limit=None, drop_zeros=True, drop_nulls=True):
    flavor_suffix = '' if flavor is None else f'-{flavor}'

    df = load_features(
        CsbIndex(glob).list_captures(),
        f'./ml-features{flavor_suffix}.h5', limit=limit)
    X = df.drop(columns='label')
    y = df['label']
    del df

    cols_before = X.shape[1]
    if drop_zeros:
        zerocols = X.columns[(X == 0).all()].to_list()
        X.drop(columns=zerocols, inplace=True)
    if drop_nulls:
        nullcols = X.columns[X.isnull().any()].to_list()  # equiv to X.dropna(axis=1)
        X.drop(columns=nullcols, inplace=True)
    cols_after = X.shape[1]

    if cols_before != cols_after:
        print(f'{X.shape[0]} rows, {cols_before} features filtered to {cols_after}')

    return X, y
