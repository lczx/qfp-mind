import numpy as np
import pandas as pd
from itertools import islice
from csb import CsbReader, pack_ip

_CATEGORY_TYPES = ['agent', 'label' , 'url', 'session_file']


def _map_ip(hosts, ip):
    ret = []
    for h, ips in hosts.items():
        if ip in ips: ret.append(h)
    return None if len(ret) == 0 else ','.join(sorted(ret))


def _map_tcp_flags(raw):
    ret = []
    if raw & 0x01: ret.append('FIN')
    if raw & 0x02: ret.append('SYN')
    if raw & 0x04: ret.append('RST')
    if raw & 0x08: ret.append('PSH')
    if raw & 0x10: ret.append('ACK')
    return ','.join(ret)


def _cap_to_df(csb_file, hosts, local_ip, cap_meta, cap, pkt_limit, sized_dir):
    limit = cap.count if pkt_limit is None else min(pkt_limit, cap.count)

    ts = np.empty(limit)
    udp = np.empty(limit, dtype=bool)
    tcp_alert = np.empty(limit, dtype=np.uint8)
    tcp_flags = np.empty(limit, dtype=object)
    sz = np.empty(limit, dtype=np.int32)
    conv = np.empty(limit, dtype=np.uint16)
    ip_src = np.empty(limit, dtype=object)
    ip_dst = np.empty(limit, dtype=object)
    port_src = np.empty(limit, dtype=np.uint16)
    port_dst = np.empty(limit, dtype=np.uint16)
    time_relative = np.empty(limit)
    time_delta = np.empty(limit)

    if hosts is not None:
        srchost = np.empty(limit, dtype=object)
        dsthost = np.empty(limit, dtype=object)

    for i, p in enumerate(cap):
        if i >= limit: break
        ts[i] = p.timestamp
        udp[i] = p.udp
        tcp_alert[i] = p.tcp_alert
        tcp_flags[i] = _map_tcp_flags(p.tcp_flags)
        sz[i] = p.size if not sized_dir or p.src_ip_raw == local_ip else -p.size
        conv[i] = p.conversation
        ip_src[i] = p.src_ip
        ip_dst[i] = p.dst_ip
        port_src[i] = p.src_port
        port_dst[i] = p.dst_port
        time_relative[i] = p.time_relative
        time_delta[i] = p.time_delta
        if hosts is not None:
            srchost[i] = _map_ip(hosts[cap.name], p.src_ip)  # type: ignore
            dsthost[i] = _map_ip(hosts[cap.name], p.dst_ip)  # type: ignore

    df = pd.DataFrame({
        'time': pd.to_datetime(ts, unit='s').tz_localize('UTC'),
        'udp': udp,
        'tcp_alert': tcp_alert,
        'tcp_flags': tcp_flags,
        'size': sz,
        'conversation': conv,
        'src_ip': ip_src,
        'dst_ip': ip_dst,
        'src_port': port_src,
        'dst_port': port_dst,
        'time_relative': time_relative,
        'time_delta': time_delta,
        'capture_id': cap_meta['c'],
        'capture_t': pd.to_datetime(cap_meta['t'], unit='ms').tz_localize('UTC'),
        'agent': cap_meta['a'],
        'agent_ver': cap_meta['v'],
        'fresh': cap_meta['f'],
        'label': cap_meta['l'],
        'url': cap_meta['u'],
        'session_file': csb_file,
        **({'src_host': srchost, 'dst_host': dsthost} if hosts is not None else {})  # type: ignore
    })

    for k in _CATEGORY_TYPES:
        df[k] = df[k].astype('category')

    return df


def build_packets_df(cap_tree, pkt_limit=None, cap_limit=None, sized_dir=False, resolve=False, stream=False, status=True):

    def merge_dfs(dfs):
        try:
            return pd.concat(dfs).reset_index()  # drop=True
        except:  # if len(dfs) is 0
            return None

    cap_items = cap_tree.items()

    def gen_dfs():
        for i, (csbf, ses_meta) in enumerate(cap_items):
            local_ipr = pack_ip(ses_meta['addr'])

            if status:
                print(f'{i+1}/{len(cap_items)}: {csbf}')  #, end='\r')

            with open(csbf, 'rb') as f:
                cr = CsbReader(f)

                hosts = cr.analysis_data['dns'] if resolve else None

                for cap in cr:
                    for cap_meta in ses_meta['captures']:
                        if cap.name == cap_meta['c']:
                            yield _cap_to_df(csbf, hosts, local_ipr, cap_meta, cap, pkt_limit, sized_dir=sized_dir)
                            # dfs.append(_cap_to_df(csbf, hosts, local_ipr, cap_meta, cap, pkt_limit, sized_dir=sized_dir))

                        # if cap_limit is not None and len(dfs) >= cap_limit:
                        #     return merge_dfs()

    df_iter = gen_dfs() if cap_limit is None else islice(gen_dfs(), cap_limit)

    return df_iter if stream else merge_dfs(df_iter)

