from .capture import build_captures_df
from .packets import build_packets_df
from .features import *


aghue_labels = ['chromium', 'chromium-cached', 'firefox', 'firefox-cached']

def add_aghue_info(df):
    df['aghue'] = df.agent.astype('object') + df.fresh.map({True: '', False: '-cached'})

def enrich_capture_df(df):
    add_aghue_info(df)
    df['total_size'] = df.uplink_size + df.downlink_size
    df['total_pkts'] = df.uplink_pkts + df.downlink_pkts
