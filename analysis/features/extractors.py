import numpy as np
import pandas as pd


def split_in_out(df):
    df_in = df[df['size'] < 0]
    df_out = df[df['size'] > 0]
    return df_in, df_out


def _pad_to_size(arr, size, value):
    return np.pad(arr, (0, size - arr.size), constant_values=value)


class FeatureExtractor:

    def __init__(self, tag, feature_names):
        self._tag = tag
        self._feature_names = feature_names
        self.nfeatures = len(feature_names)

    @property
    def feature_names(self):
        return [ f'{self._tag}.{fn}' for fn in self._feature_names ]


class CountsFeatureExtractor(FeatureExtractor):

    def __init__(self):
        fnames = [ 'total', 'out', 'in', 'out_ratio', 'in_ratio' ]
        super(CountsFeatureExtractor, self).__init__('counts', fnames)

    def extract(self, df, df_in, df_out, shared) -> np.ndarray:
        ret = np.empty(self.nfeatures)

        ret[0] = df.shape[0]  # packet count
        ret[1] = df_out.shape[0]  # outgoing packet count
        ret[2] = df_in.shape[0]  # incoming packet count
        ret[3] = ret[1] / ret[0]  # outgoing ratio
        ret[4] = ret[2] / ret[0]  # incoming ratio

        return ret


class TimeFeatureExtractor(FeatureExtractor):

    def __init__(self):
        statnames = [ 'iat_max', 'iat_mean', 'iat_std', 'iat_3q' ]
        fnames = [ *[ f'{a}{b}' for a in ['', 'in_', 'out_'] for b in statnames ], 'rtime_q1', 'rtime_q2', 'rtime_q3', 'tot_time' ]
        super(TimeFeatureExtractor, self).__init__('time', fnames)

    def extract(self, df, df_in, df_out, shared) -> np.ndarray:
        dfa = pd.DataFrame(df)
        dfa['iat'] = df.time.diff()  # Inter-arrival time
        dfa['rtime'] = df.time - df.time[0]  # Relative packet time

        dfa_in, dfa_out = split_in_out(dfa)
        # debug: print(len(dfa), len(dfa_in), len(dfa_out))

        ret = np.empty(self.nfeatures)

        # Computes inter-arrival-time maximum, mean, standard deviation and 3rd quartile
        # Note: we may try to calculate inter-arrival-time after splitting
        def compute_iat_stats(offset, df):
            try:
                mx = df['iat'].max()
                ret[offset] = mx.delta if pd.notna(mx) else 0

                mean = df['iat'].mean()
                ret[offset + 1] = mean.delta if pd.notna(mean) else 0

                std = df['iat'].std()
                ret[offset + 2] = std.delta if pd.notna(std) else 0

                q3 = df['iat'].quantile(.75)
                ret[offset + 3] = q3.delta if pd.notna(q3) else 0

            except AttributeError as e:
                print(90 * '-')
                print(df)
                print(90 * '-')
                raise e

        compute_iat_stats(0, dfa)
        compute_iat_stats(4, dfa_in)
        compute_iat_stats(8, dfa_out)

        # Compute 1st, 2nd (median) and 3rd quartiles of packet timestamps
        rtime_qs = pd.to_numeric(df['rtime']).quantile([.25, .50, .75])
        ret[12] = rtime_qs.iat[0]
        ret[13] = rtime_qs.iat[1]
        ret[14] = rtime_qs.iat[2]

        # Total transmission time
        ret[15] = df.rtime.iat[-1].delta

        return ret


class TranspositionFeatureExtractor(FeatureExtractor):

    def __init__(self, count=300):
        fnames = [ f'{p}{n}' for p in ['in_', 'out_'] for n in range(count) ]
        super(TranspositionFeatureExtractor, self).__init__('trans', fnames)
        self.count = count

    def extract(self, df, df_in, df_out, shared):
        count = self.count

        # The "index" column is the nr. of total packets in the capture before the current one
        tr_in = df_in['index'].iloc[1:count+1].to_numpy()
        tr_out = df_out['index'].iloc[1:count+1].to_numpy()

        # Warn! We use NaNs here to pad (thus the arrays are casted to float first), we should
        # find a way to make the classifier ignore them in the distance function, see Wang et al.
        tr_in = _pad_to_size(tr_in.astype(float), count, np.nan)
        tr_out = _pad_to_size(tr_out.astype(float), count, np.nan)

        # Note: we may also want to compute mean and stddev, according to k-FP paper, WATCH OUT FOR NANs
        return np.concatenate([tr_in, tr_out])


class IntervalFeatureExtractor(FeatureExtractor):

    def __init__(self, i1_count=300, i2_count=300):
        counts = [
            *[ f'i1_{n}' for n in range(i1_count) ],
            *[ f'i2_{n}' for n in range(i2_count) ],
            *[ f'i3_{n}' for n in range(3) ]
        ]
        fnames = [ f'{a}{b}' for a in ['in_', 'out_'] for b in counts ]
        super(IntervalFeatureExtractor, self).__init__('ival', fnames)
        self.i1_count = i1_count
        self.i2_count = i2_count

    def extract(self, df, df_in, df_out, shared):
        _, i1_in, i2_in, i3_in = self._calc_interval_feats(df_in, self.i1_count, self.i2_count)
        intervals_out, i1_out, i2_out, i3_out = self._calc_interval_feats(df_out, self.i1_count, self.i2_count)

        shared['intervals_out'] = intervals_out
        return np.concatenate([i1_in, i1_out, i2_in, i2_out, i3_in, i3_out])

    def _calc_interval_feats(self, df, i1_count, i2_count):
        # An interval is the number of packets between two packets in the same direction

        # Compute the differences on the index, subtract 1 to not account for the increasing index
        # The first interval is simply the index of the first packet in this direction
        intervals = df['index'].diff() - 1
        intervals.iat[0] = df.iat[0, df.columns.get_loc('index')]
        intervals = intervals.astype(int)

        # Interval-I: the intervals for the first N packets in each direction
        i1 = intervals[:i1_count].to_numpy()
        i1 = _pad_to_size(i1.astype(float), i1_count, np.nan)

        # Interval-II: of a vector of size N, the i-th entry is the count of intervals of size i,
        # the ones with more than N packets are counted as containing N packets
        i2 = np.zeros(i2_count, dtype=int)
        for sz, count in intervals.value_counts().iteritems():
            i2[min(sz, i2_count - 1)] += count

        # Interval-III: aggregate 3-5th, 6-8th and 9-13th entries from Interval-II
        i3 = np.array([ np.sum(i2[2:5]), np.sum(i2[5:8]), np.sum(i2[8:13]) ], dtype=int)

        return intervals, i1, i2, i3


class BurstFeatureExtractor(FeatureExtractor):

    def __init__(self):
        fnames = [ 'max', 'mean', 'count', 'count_gt5', 'count_gt10', 'count_gt20' ]
        super(BurstFeatureExtractor, self).__init__('burst', fnames)

    def extract(self, df, df_in, df_out, shared):
        # A busts is the number of packets in each sequence of outgoing packets with no two adjacent incoming packets
        bursts = []
        acc = 0

        for v in (shared['intervals_out'] < 2):
            if not v and acc > 0:
                bursts.append(acc)
                acc = 0
            acc += 1
        if acc > 0: bursts.append(acc)

        bursts = np.array(bursts)

        ret = np.empty(self.nfeatures)
        ret[0] = bursts.max()
        ret[1] = bursts.mean()
        ret[2] = bursts.size
        ret[3] = np.count_nonzero(bursts > 5)
        ret[4] = np.count_nonzero(bursts > 10)
        ret[5] = np.count_nonzero(bursts > 20)
        return ret


class DistributionFeatureExtractor(FeatureExtractor):

    def __init__(self, chunk_size=30, count=200):
        fnames = [
            *[ f'c{i}' for i in range(count) ],
            *[ 'c_std', 'c_mean', 'c_median', 'c_max' ],
            *[ f'cg{i}' for i in range(count // 10) ]
        ]
        super(DistributionFeatureExtractor, self).__init__('dist', fnames)
        self.chunk_size = chunk_size
        self.count = count

    def extract(self, df, df_in, df_out, shared):
        # Divide the packet sequence into non-overlapping chunks of N packets, count the number of outgoing
        # packets in the first M chunks as features, ignore extra chunks if any, zero-pad if less than M chunks.
        chunks = (df['size'] > 0).groupby(df.index // self.chunk_size).agg('sum').to_numpy()[:self.count]
        chunks = _pad_to_size(chunks, self.count, 0)

        # Split and sum-aggregate subsets (20) of length 10
        chunks_groups = np.sum(np.split(chunks, self.count // 10), axis=1)

        # Note: and if we try to compute statistics without padding?

        ret = np.empty(self.nfeatures)
        ret[0:self.count] = chunks
        ret[self.count + 0] = chunks.std()
        ret[self.count + 1] = chunks.mean()
        ret[self.count + 2] = np.median(chunks)
        ret[self.count + 3] = chunks.max()
        ret[self.count + 4:self.count + 4 + self.count // 10] = chunks_groups

        return ret


class FirstLastFeatureExtractor(FeatureExtractor):

    def __init__(self, first_sizes_count=20, firstlast_inout_count=30):
        fnames = [
            *[ f'f{first_sizes_count}_s{i}' for i in range(first_sizes_count) ],
            *[ f'f{first_sizes_count}_d{i}' for i in range(first_sizes_count) ],
            *[ f'{d}{firstlast_inout_count}_{s}' for d in ['f', 'l'] for s in ['in', 'out'] ]
        ]
        super(FirstLastFeatureExtractor, self).__init__('fl', fnames)
        self.first_sizes_count = first_sizes_count
        self.firstlast_inout_count = firstlast_inout_count

    def extract(self, df, df_in, df_out, shared):
        ret = np.empty(self.nfeatures)
        szc = self.first_sizes_count

        # Size and direction of first N packets
        f20_signed_sizes = _pad_to_size(
                df.loc[:szc-1, 'size'].to_numpy().astype(float), szc, np.nan)
        ret[0:szc] = np.abs(f20_signed_sizes)
        ret[szc:2*szc] = np.sign(f20_signed_sizes)

        # Incoming and outgoing packet counts for the first and last M packets
        f30 = df['size'][:self.firstlast_inout_count]
        ret[2*szc + 0] = np.count_nonzero(f30 < 0)
        ret[2*szc + 1] = np.count_nonzero(f30 > 0)

        l30 = df['size'][-self.firstlast_inout_count:]
        ret[2*szc + 2] = np.count_nonzero(l30 < 0)
        ret[2*szc + 3] = np.count_nonzero(l30 > 0)

        return ret


class PPSFeatureExtractor(FeatureExtractor):

    def __init__(self, seconds=100, subsets=20):
        fnames = [
            *[ f's{i}' for i in range(seconds) ],
            *[ 's_std', 's_mean', 's_median', 's_min', 's_max' ],
            *[ f'sg{i}' for i in range(subsets) ]
        ]
        super(PPSFeatureExtractor, self).__init__('pps', fnames)
        self.seconds = seconds
        self.subsets = subsets

    def extract(self, df, df_in, df_out, shared):
        # The number of packets transmitted in each second for the first N seconds, zero-pad to N if needed
        pps = df.set_index(['time'])['index'].resample('s', origin='start').count().to_numpy()[:self.seconds]
        pps = _pad_to_size(pps, self.seconds, 0)

        # Split in 20 subsets and compute the total for each
        pps_groups = np.sum(np.split(pps, self.subsets), axis=1)

        # Note: and if we try to compute statistics without padding?

        ret = np.empty(self.nfeatures)
        ret[0:self.seconds] = pps
        ret[self.seconds + 0] = pps.std()
        ret[self.seconds + 1] = pps.mean()
        ret[self.seconds + 2] = np.median(pps)
        ret[self.seconds + 3] = pps.min()
        ret[self.seconds + 4] = pps.max()
        ret[self.seconds + 5:self.seconds + 5 + self.subsets] = pps_groups
        return ret


class CumulFeatureExtractor(FeatureExtractor):

    def __init__(self, points=100):
        fnames = [ f'p{i}' for i in range(points) ]
        super(CumulFeatureExtractor, self).__init__('cumul', fnames)

    def extract(self, df, df_in, df_out, shared):
        # A sample of N points from the piecewise linear interpolation of the cumulative sum of packet sizes
        # Note: should we also try with absolute values?
        pkt_sizes = np.pad(-df['size'].to_numpy(), (1, 0))
        cum_src = np.cumsum(pkt_sizes)

        cum = np.interp(np.linspace(0, 1, self.nfeatures + 1), np.linspace(0, 1, cum_src.size), cum_src)

        # Note: actually used points + 1 and discarded the 1st point to have nicer values
        return cum[1:]
