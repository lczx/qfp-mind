import sys
import random
import multiprocessing
import traceback
import numpy as np

from .extractors import *
from ..packets import build_packets_df


class AllFeaturesExtractor:

    _extractors = [
        CountsFeatureExtractor(), TimeFeatureExtractor(), TranspositionFeatureExtractor(), IntervalFeatureExtractor(),
        BurstFeatureExtractor(), DistributionFeatureExtractor(), FirstLastFeatureExtractor(), PPSFeatureExtractor(),
        CumulFeatureExtractor()
    ]

    def get_feature_names(self):
        return np.concatenate([e.feature_names for e in self._extractors])

    def extract(self, df):
        if df['capture_id'].drop_duplicates().size != 1:
            raise ValueError('DataFrame must hold a single capture')

        df = df.reset_index()
        df_in, df_out = split_in_out(df)

        shared = {}
        return np.concatenate([e.extract(df, df_in, df_out, shared) for e in self._extractors])


_hdf_key = lambda session: f'/{session["host"]}/s_{session["sid"].replace("-", "_")}'


def sort_csbf(csb_list, seed):
    base = []
    amp0 = []
    amp1 = []

    list = sorted(csb_list)
    if seed is not None:
        random.Random(seed).shuffle(list)

    for f in list:
        if '/base/' in f: base.append(f)
        if '/amp0/' in f: amp0.append(f)
        if '/amp1/' in f: amp1.append(f)

    ret = []

    if len(base) != 2 * len(amp0) or len(base) != 2 * len(amp1):
            print('Warn: not all capture sets are complete', file=sys.stderr)

    while len(base) > 0 and len(amp0) > 0 and len(amp1) > 0:
        if len(base) > 0: ret.append(base.pop())
        if len(amp0) > 0: ret.append(amp0.pop())
        if len(base) > 0: ret.append(base.pop())
        if len(amp1) > 0: ret.append(amp1.pop())

    return ret


def _feat_extract_kernel(extractor, cap_filter, csb_file, session, idx):
    feat_names = extractor.get_feature_names()

    dfs = build_packets_df({csb_file: session}, \
            sized_dir=True, stream=True, status=False)

    #tot_caps = len(session['captures'])

    feats = []
    for i, d in enumerate(dfs):
        label, cap_id = d.loc[0, ['label', 'capture_id']]
        if cap_filter is not None:
            d = cap_filter(d)
        # debug: print(csb_file, cap_id, label)
        #msg = f'\x1b[2K{100 * (i + 1) / tot_caps:.2f}% {i + 1}/{tot_caps} {cap_id} {label}'
        #print(f'{msg:<120}', end='\r')
        feats.append((extractor.extract(d), label, cap_id))
    feats = list(zip(*feats))

    X = pd.DataFrame(feats[0], columns=feat_names)
    y = pd.Series(feats[1], name='label')
    c = pd.Series(feats[2], name='capture')
    df = pd.concat([X, y, c], axis=1).set_index('capture')
    return idx, csb_file, _hdf_key(session), df


def store_features(csb_index, hdf_file, extractor, cap_filter=None, n_jobs=None):
    # Extract for all captures
    cap_tree = csb_index.list_captures()
    hs = pd.HDFStore(hdf_file)
    hs_keys = hs.keys()

    to_extract = [f for f, s in cap_tree.items() if _hdf_key(s) not in hs_keys]

    def callback(data):
        i, csb_file, key, df = data
        print(f'{i + 1:>3}/{len(to_extract)}: {csb_file} {df.shape}')
        hs.put(key, df)

    def err_callback(e):
        traceback.print_exception(type(e), e, e.__traceback__)

    with multiprocessing.Pool(n_jobs) as pool:
        for i, csb_file in enumerate(to_extract):
            params = (extractor, cap_filter, csb_file, cap_tree[csb_file], i)
            pool.apply_async(_feat_extract_kernel, params, callback=callback, error_callback=err_callback)
        pool.close()
        pool.join()

    hs.close()


def load_features(csb_tree, hdf_file, limit=None, seed=42):
    sessions = [csb_tree[k] for k in sort_csbf(csb_tree.keys(), seed)]
    if limit is not None:
        if limit % 4 != 0:
            print('Limit is not multiple of 4, there will be uneven representation of URLs among captures', file=sys.stderr)
        sessions = sessions[0:limit]

    hs = pd.HDFStore(hdf_file, 'r')

    tot_captures = 0

    dfs = []
    for i, session in enumerate(sessions):
        print(f'{i + 1:>3}/{len(sessions)}', end='\r')
        key = _hdf_key(session)

        rows = [c['c'] for c in session['captures']]
        tot_captures += len(rows)

        dfs.append(hs.select(key).loc[rows, :])

    hs.close()

    ret = pd.concat(dfs)
    assert(tot_captures == ret.shape[0])
    return ret
