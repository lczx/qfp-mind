import pandas as pd
from csb import CsbReader
from csb.util import pack_ip


_CAPTURES_DF_COL_REMAP = {
    'c': 'cid',
    't': 'timestamp',
    'a': 'agent',
    'v': 'agent_ver',
    'f': 'fresh',
    'l': 'label',
    'u': 'url'
}


def _get_cap_metrics(cap, local_ip):
    up_n = 0
    up_sz = 0
    down_n = 0
    down_sz = 0
    for pkt in cap:
        if pkt.src_ip_raw == local_ip:
            up_n += 1
            up_sz += pkt.size
        else:
            down_n += 1
            down_sz += pkt.size
    return up_n, up_sz, down_n, down_sz


def _fetch_cap_details(csbf, ids, local_ip, packet_counts, inspect):
    def getter(c):
        ret = { 'c': c.name }
        if packet_counts:
            ret['pkt_count'] = c.count
        if inspect:
            up_n, up_sz, down_n, down_sz = _get_cap_metrics(c, local_ip)
            ret['uplink_pkts'] = up_n 
            ret['uplink_size'] = up_sz 
            ret['downlink_pkts'] = down_n 
            ret['downlink_size'] = down_sz 
        return ret

    with open(csbf, 'rb') as f:
        cr = CsbReader(f)
        return pd.DataFrame(getter(c) for c in cr if c.name in ids)


def build_captures_df(cap_tree, packet_counts=False, inspect=False, status=True):
    sessions = cap_tree.items()

    def iter_csbf():
        for i, (csbf, ses_meta) in enumerate(sessions):
            meta = { k: v for k, v in ses_meta.items() if k != 'captures' }
            data = [ { **meta, **c } for c in ses_meta['captures'] ]

            df = pd.DataFrame(data)

            if packet_counts or inspect:
                ids = [c['c'] for c in ses_meta['captures']]
                local_ip = pack_ip(meta['addr'])
                adds = _fetch_cap_details(csbf, ids, local_ip, packet_counts, inspect)
                df = df.join(adds.set_index('c'), on='c')

            if status:
                print(f'{i+1}/{len(sessions)}: {csbf}', end='\r')
    
            yield df

    df = pd.concat(iter_csbf()).reset_index(drop=True)
    df['t'] = pd.to_datetime(df['t'], unit='ms').dt.tz_localize('UTC')
    return df.rename(columns=_CAPTURES_DF_COL_REMAP)
