#!/bin/bash
hash docker 2> /dev/null && DOCKER_CMD="sudo docker"
hash podman 2> /dev/null && DOCKER_CMD=podman

if [ -z "$DOCKER_CMD" ]; then
    echo 'No supported container runtime has been found' >&2
    exit 1
fi

OVERRIDES_FILE=$(mktemp -u)

cat > "$OVERRIDES_FILE" <<EOF
{
  "@jupyterlab/apputils-extension:themes": {
    "theme": "JupyterLab Dark"
  }
}
EOF

$DOCKER_CMD run --rm -it --name jupyter -p 8888:8888 \
    -v "$(realpath "${0%/*}"):/home/jovyan/work:Z" \
    -v "$OVERRIDES_FILE:/opt/conda/share/jupyter/lab/settings/overrides.json:Z" \
    --security-opt label=disable \
    jupyter/scipy-notebook
