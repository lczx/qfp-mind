#!/bin/bash
set -e

SCRIPTDIR="$(dirname "$0")"
[ -f "$SCRIPTDIR/.env" ] && source "$SCRIPTDIR/.env"

dirs=$(find . -mindepth 1 -maxdepth 1 -type d \
    | grep -E '^\./[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$')

for d in $dirs; do
    echo "Processing \"$d\"..." >&2

    if [ ! -f "$d/analysis.json" ]; then
        if [ -z "$REAPER_DIR" ]; then
            echo 'A capture without DNS analysis has been found and no Reaper directory was specified' >&2
            exit 1
        fi
        "$REAPER_DIR/reaper" analyze dns "$d"
    fi

    "$SCRIPTDIR/dns-filterlist.py" "$d" > "$d.filtips"
    "$SCRIPTDIR/dns-report.py" "$d" > "$d.dnsrep"

    memtotal=$(( $(grep MemTotal /proc/meminfo | grep -o '[[:digit:]]*') / 1024 - 2048 ))
    node --max-old-space-size=$memtotal "$SCRIPTDIR/capt-summary/index.mjs" "$d"

    # use tee to show output
    filtout=$("$SCRIPTDIR/filter-ips.py" "$d.filtips" "$d.csb" 2>&1 | tee /dev/stderr | grep filtered || true)
    if [ -z "$filtout" ]; then
        echo 'Filtered out nothing'
        rm "${d}_f.csb"
    else
        mv "${d}.csb" "${d}_cap.csb"
        mv "${d}_f.csb" "${d}.csb"
    fi

    tar --zstd -cf "$d.tar.zst" "${d#./}" && rm -r "$d"
done

for f in *.tar.zst; do
    id="${f%.tar.zst}"

    host=$(tar -xOf "$f" "$id/session.log" | head -n1 | grep -Po '(?<=")(\w+)(?=")')
    urlf=$(tar -xOf "$f" "$id/config.json" | jq -r .urls)

    if [[ "$urlf" == *-a0.rpurls ]]; then dest=$host/amp0
    elif [[ "$urlf" == *-a1.rpurls ]]; then dest=$host/amp1
    else dest=$host/base
    fi

    echo "\"$id\" goes to \"$dest\""
    mkdir -p "$dest"
    mv "$id"* "$dest/"
done
