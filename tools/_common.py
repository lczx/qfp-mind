def load_filter(fname):
    items = []
    with open(fname, encoding='utf8') as f:
        for l in f:
            line = l.strip()
            if line.startswith('#') or line == '': continue
            items.append(line)
    return items

