#!/bin/bash
SCRIPTDIR="$(dirname "$0")"
[ -f "$SCRIPTDIR/.env" ] && source "$SCRIPTDIR/.env"

if [[ -z "$RSYNC_REMOTE" ]]; then
    echo 'RSYNC_REMOTE is not defined' >&2
    exit 1
fi

RSYNC_ARGS=(
    -av --progress \
    -f '- *_cap.csb' -f '+ *.csb' -f '- _*' -f '+ */' -f '- *'
)

rsync "${RSYNC_ARGS[@]}" "${RSYNC_REMOTE}captures/" "$SCRIPTDIR/../captures/"
rsync "${RSYNC_ARGS[@]}" "${RSYNC_REMOTE}captures-unmon/" "$SCRIPTDIR/../captures-unmon/"
