import os from 'os';
import { createReadStream, createWriteStream, existsSync, statSync } from 'fs';
import { readdir, readFile } from 'fs/promises';
import { spawn } from 'child_process';
import path from 'path';
import { PassThrough } from 'stream';

import chalk from 'chalk';
import tar from 'tar-stream';
import { ZSTDCompress } from 'simple-zstd';

const TSHARK_BINARY = '/usr/bin/tshark';
const TSHARK_FILTER = 'tcp.port == 443 || udp.port == 443';

const PROTO_ICMP = 1;
const PROTO_TCP = 6;
const PROTO_UDP = 17;


const isDirectory = path => existsSync(path) && statSync(path).isDirectory();


const getProcessOutput = (command, args) => new Promise((res, rej) => {
  const proc = spawn(command, args);
  let procOut = '';
  proc.stdout.on('data', chunk => procOut += chunk.toString());
  proc.on('close', code => code === null || code === 0
    ? res(procOut)
    : rej(`Process exited with code ${code}`));
  proc.on('error', rej);
});


const readFirstLine = (filename) => new Promise(res => {
  const stream = createReadStream(filename, { flags: 'r', encoding: 'utf-8' });
  let fileData = '';
  stream.on('data', d => {
    fileData += d;
    const nlIndex = fileData.indexOf('\n');
    if (nlIndex !== -1) {
      stream.destroy();
      res(fileData.slice(0, nlIndex));
    }
  });
});


const encodeIp = (str) => {
  const [a, b, c, d] = str.split('.').map(w => parseInt(w));
  return a << 24 | b << 16 | c << 8 | d;
};


//const decodeIp = (nr) =>
//  `${nr >> 24 & 0xff}.${nr >> 16 & 0xff}.${nr >> 8 & 0xff}.${nr & 0xff}`;


async function fetchPacketData(captureFile) {
    const args = ['-r', captureFile, '-Y', TSHARK_FILTER, '-T', 'json'];
    return JSON.parse(await getProcessOutput(TSHARK_BINARY, args));
}


function getTcpAlertId(a) {
  if (a == null) return 0;

  // epan/dissectors/packet-tcp.c
  if ('tcp.analysis.retransmission' in a) return 1;
  if ('tcp.analysis.out_of_order' in a) return 2;
  if ('tcp.analysis.lost_segment' in a) return 3;
  if ('tcp.analysis.window_update' in a) return 4;
  if ('tcp.analysis.window_full' in a) return 5;
  if ('tcp.analysis.keep_alive' in a) return 6;
  if ('tcp.analysis.keep_alive_ack' in a) return 7;
  if ('tcp.analysis.zero_window_probe' in a) return 8;
  if ('tcp.analysis.zero_window' in a) return 9;
  if ('tcp.analysis.ack_lost_segment' in a) return 10;
  if ('tcp.analysis.reused_ports' in a) return 11;
  throw new Error(JSON.stringify(a));
}


function encodeSummary(s) {
  const arr = new Uint8Array(42);
  const dv = new DataView(arr.buffer);

  dv.setFloat64(0, s.timestamp);
  dv.setUint8(8, s.isUdp << 4 | getTcpAlertId(s.alerts));
  dv.setUint8(9, s.tcpFlags);
  dv.setUint16(10, s.size);
  dv.setUint16(12, s.conversation);
  dv.setUint32(14, s.srcIp);
  dv.setUint32(18, s.dstIp);
  dv.setUint16(22, s.srcPort);
  dv.setUint16(24, s.dstPort);
  dv.setFloat64(26, s.timeRelative)
  dv.setFloat64(34, s.timeDelta);
  return arr;
}


function makePacketSummary(packetData) {
  const l = packetData._source.layers;
  const proto = parseInt(l.ip['ip.proto']);

  if (proto === PROTO_ICMP) {
    //console.warn('ICMP', l.icmp['icmp.type'], l.icmp['icmp.code']);
    return null;
  }

  if (proto !== PROTO_TCP && proto !== PROTO_UDP) {
    console.warn('Unknown protocol:', proto);
    return null;
  }

  const alerts = l?.tcp?.['tcp.analysis']?.['tcp.analysis.flags']?.['_ws.expert'];

  const timeRelative = parseFloat(proto === PROTO_UDP
    ? l.udp['Timestamps']['udp.time_relative'] : l.tcp['Timestamps']['tcp.time_relative'])

  const timeDelta = parseFloat(proto === PROTO_UDP
    ? l.udp['Timestamps']['udp.time_delta'] : l.tcp['Timestamps']['tcp.time_delta']);

  // TCP flags: FIN 0x01, SYN 0x02, RST 0x04, PSH 0x08, ACK 0x16
  const tcpFlags = proto === PROTO_UDP ? 0 : parseInt(l.tcp['tcp.flags']);

  return encodeSummary({
    timestamp: parseFloat(l.frame['frame.time_epoch']),
    size: parseInt(l.ip['ip.len']) - parseInt(l.ip['ip.hdr_len']),
    conversation: parseInt(proto === PROTO_UDP ? l.udp['udp.stream'] : l.tcp['tcp.stream']),
    isUdp: proto === PROTO_UDP,
    srcIp: encodeIp(l.ip['ip.src']),
    dstIp: encodeIp(l.ip['ip.dst']),
    srcPort: parseInt(proto === PROTO_UDP ? l.udp['udp.srcport'] : l.tcp['tcp.srcport']),
    dstPort: parseInt(proto === PROTO_UDP ? l.udp['udp.dstport'] : l.tcp['tcp.dstport']),
    timeRelative,
    timeDelta,
    tcpFlags,
    alerts
  });
}


async function readCaptureData(sessionDir, captId) {
  const captDir = path.join(sessionDir, captId);
  const packets = (await fetchPacketData(path.join(captDir, 'packets.pcapng')))
    .map(makePacketSummary)
    .filter(p => p !== null);
  return packets;
}


async function executeConcurrently(taskParams, executor, onProgress, onError, concurrency) {
  const params = taskParams.slice();
  const tasks = [];
  const maxTasks = concurrency || os.cpus().length;

  onProgress?.('start', maxTasks);

  let nextTaskId = 1;
  do {
    while (tasks.length < maxTasks) {
      const newParams = params.pop();
      if (newParams == null) break;

      const taskId = nextTaskId++;
      tasks.push({
        id: taskId,
        par: newParams,
        prom: executor(newParams).then(() => taskId).catch(onError)
      });
    }

    onProgress?.('update', maxTasks, tasks, taskParams.length);

    if (tasks.length === maxTasks || params.length === 0) {
      const id = await Promise.any(tasks.map(p => p.prom));
      const idx = tasks.findIndex(p => p.id === id);
      tasks.splice(idx, 1);
    }
  } while (tasks.length > 0 || params.length > 0);

  onProgress?.('end', maxTasks);
}


async function saveBundle(outFile, sessionMeta, analysisData, data, onUpdate) {
  const pack = tar.pack();

  const packPromise = new Promise((res, rej) => {
    const tarBufs = [];
    pack
      .on('data', b => tarBufs.push(b))
      .on('error', rej)
      .on('end', () => res(Buffer.concat(tarBufs)));
  });
  const packFile = (name, buffer) =>
    new Promise((res, rej) =>
      pack.entry({ name, mtime: new Date(0) }, buffer, err => err ? rej(err) : res()));

  await packFile('_session', sessionMeta);
  await packFile('_analysis', analysisData);
  const dataKeys = Object.keys(data).sort();

  for (let i = 0; i < dataKeys.length; ++i) {
    const name = dataKeys[i];
    const packets = data[dataKeys[i]];

    const pBuf = packets.map(p => Buffer.from(p));
    await packFile(name, Buffer.concat(pBuf));

    onUpdate?.(i, dataKeys.length, name, packets.length);
  }

  pack.finalize();

  // First create the whole tar archive in memory so we can pass its size to zstd
  const tarBuf = await packPromise;

  await new Promise((res, rej) => {
    new PassThrough().end(tarBuf)
      .pipe(ZSTDCompress(9, undefined, undefined, ['--stream-size=' + tarBuf.length]))
      .pipe(createWriteStream(outFile))
      .on('error', rej)
      .on('finish', res);
  });
}


if (process.argv.length < 3) {
  console.error(`Usage: ${path.basename(process.argv[1])} session_dir`);
  process.exit(1);
}

const sessionDir = process.argv[2];

if (!isDirectory(sessionDir)) {
  console.error(`"${sessionDir}" is not a valid session directory`);
  process.exit(1);
}

(async () => {
  const line = await readFirstLine(path.join(sessionDir, 'session.log'))
  const analysisData = await readFile(path.join(sessionDir, 'analysis.json'));
  const [, s, h, a, i] = /^\S+ info: Started session (.*) on "(.*)" (.*) \((.*)\)$/.exec(line);

  const captIds = (await readdir(sessionDir)).filter(d => isDirectory(path.join(sessionDir, d)));

  let captures = [];
  for (const captId of captIds) {
    const captDir = path.join(sessionDir, captId);
    const meta = JSON.parse(await readFile(path.join(captDir, 'metadata.json')));
    captures.push({
      c: captId,
      t: meta.timestamp,
      a: meta.agent.type,
      v: meta.agent.version,
      f: meta.freshProfile,
      l: meta.metadata.label,
      u: meta.remote.url,
    });
  }

  const packetData = {};

  const processCapture = async (meta) => {
    const packets = await readCaptureData(sessionDir, meta.c);
    packetData[meta.c] = packets;
  };

  const onProgress = (state, concurrentTasks, tasks, total) => {
    if (state === 'start') {
      process.stdout.write('\n'.repeat(concurrentTasks));

    } else if (state === 'end') {
      process.stdout.write(`\x1b[${concurrentTasks}A\x1b[2K`);  // Erase line

    } else if (state === 'update') {
      process.stdout.write(`\x1b[${concurrentTasks}F`);

      for (let i = 0; i < concurrentTasks; ++i) {
        const t = tasks[i];
        const text = t == null ? '' : (() => {
          const stat = chalk.yellowBright(`${t.id.toString().padStart(3)}/${total}`);
          const id = chalk.blueBright(t.par.c);
          const det = `${t.par.a}@${t.par.v} ${t.par.f ? ' ' : '*'}`;
          const url = chalk.greenBright(t.par.u);
          return `${stat}: ${id} ${det} ${url}`;
        })();

        console.log(`\x1b[K${text}`);
      }
    }
  }

  await executeConcurrently(captures, processCapture, onProgress, e => {
    console.error(e);
    process.exit(1);
  });

  // CSB: Capbure Summary Bundle
  const outFile = path.join(path.dirname(sessionDir), `${s}.csb`);
  const sessionMeta = JSON.stringify({ s, h, a, i, c: captures });

  console.log('Saving data...\n')
  await saveBundle(outFile, sessionMeta, analysisData, packetData, (i, n, name, pkts) => {
    const perc = ((i + 1) / n * 100).toFixed(2).padStart(6);
    const text =`\x1b[1A${perc}% ${name} (${pkts} packets)`;
    console.log(text.padEnd(process.stdout.columns));
  });
  console.log('Done');
})();
