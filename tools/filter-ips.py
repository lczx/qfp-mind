#!/usr/bin/env python
from io import BytesIO
import sys
import os

sys.path.append(os.path.dirname(__file__) + '/..')

from _common import load_filter
from csb import CsbReader, CsbWriter, pack_ip


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print(f'Usage: {os.path.basename(sys.argv[0])} filter_file csb_file', file=sys.stderr)
        sys.exit(1)

    filter_file = sys.argv[1]
    csb_file = sys.argv[2]

    blacklist = [pack_ip(i) for i in load_filter(filter_file)]
    
    with open(csb_file, 'rb') as f:
        cr = CsbReader(f)
        cw = CsbWriter(cr.metadata, cr.analysis_data)

        for capture in cr:
            out_cap = BytesIO()

            filt_count = 0
            for p in capture:
                if p.src_ip_raw in blacklist or p.dst_ip_raw in blacklist:
                    filt_count += 1
                else:
                    out_cap.write(p.to_bytes())
            cw.add_data(capture.name, out_cap.getvalue())

            if filt_count > 0:
                print(f'{capture.name}: filtered {filt_count} packets', file=sys.stderr)
            
    in_base, _ = os.path.splitext(csb_file)

    with open(in_base + '_f.csb', 'wb') as f:
        f.write(cw.write())
