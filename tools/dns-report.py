#!/usr/bin/env python
import sys
import os
import json
import collections


def read_capt_meta(session_dir, capt_id):
    with open(os.path.join(session_dir, capt_id, 'metadata.json')) as f:
        return json.load(f)


def filter_meta(agent_type, fresh):
    return lambda m: m['agent']['type'] == agent_type and m['freshProfile'] == fresh


def count_hostnames(ids, dns_data, n=None):
    cnt = collections.Counter()
    for cid in ids:
        capt_dns = dns_data[cid].keys()
        cnt.update(capt_dns)
    return cnt.most_common(n)


def show_popular_hosts(label, capt_ids, dns_data):
    print(f'{label} ({len(capt_ids)} captures):')
    for k, v in count_hostnames(capt_ids, dns_data, 10):
        print('  %4d  %s' % (v, k))
    print()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f'Usage: {os.path.basename(sys.argv[0])} session_dir', file=sys.stderr)
        sys.exit(1)

    session_dir = sys.argv[1]

    captids = [f for f in os.listdir(session_dir) if os.path.isdir(os.path.join(session_dir, f))]

    capt_data = {cid: read_capt_meta(session_dir, cid) for cid in captids}

    with open(os.path.join(session_dir, 'analysis.json')) as f:
        dns_data = json.load(f)['dns']

    fx_ids = [cid for cid, meta in capt_data.items() if filter_meta('firefox', True)(meta)]
    show_popular_hosts('Firefox', fx_ids, dns_data)
    fx_cache_ids = [cid for cid, meta in capt_data.items() if filter_meta('firefox', False)(meta)]
    show_popular_hosts('Firefox cached', fx_cache_ids, dns_data)

    chr_ids = [cid for cid, meta in capt_data.items() if filter_meta('chromium', True)(meta)]
    show_popular_hosts('Chromium', chr_ids, dns_data)
    chr_cache_ids = [cid for cid, meta in capt_data.items() if filter_meta('chromium', False)(meta)]
    show_popular_hosts('Chromium cached', chr_cache_ids, dns_data)
