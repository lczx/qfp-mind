#!/usr/bin/env python
import sys
import os
import json
from _common import load_filter


FILTER_FILE = os.path.dirname(os.path.realpath(__file__)) + '/dns-filterlist'


def number_ip(ip):
    a, b, c, d = ip.split('.')
    return int(a) << 24 | int(b) << 16 | int(c) << 8 | int(d)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f'Usage: {os.path.basename(sys.argv[0])} session_dir')
        sys.exit(1)

    session_dir = sys.argv[1]
    with open(os.path.join(session_dir, 'analysis.json')) as f:
        dns_data = json.load(f)['dns']

    blacklist = load_filter(FILTER_FILE)
    ips = {}

    for cid, capt_dns in dns_data.items():
        for b in blacklist:
            if b in capt_dns:
                if b not in ips: ips[b] = set()
                ips[b].update(capt_dns[b])

    hosts = sorted(ips.keys())

    for h in hosts:
        print('# ' + h)
        for i in sorted(ips[h], key=number_ip): print(i)
        print()
