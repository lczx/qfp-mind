import glob
from operator import itemgetter
from .reader import CsbReader


_META_KEY_MAP = {'agent': 'a', 'agent_ver': 'v',
                 'fresh': 'f', 'label': 'l', 'url': 'u'}


def _match_filt(spec, c):
    def match(ent, val):
        if type(val) == list:
            return ent in val
        return ent == val

    return all(match(c[_META_KEY_MAP[k]], v) for k, v in spec.items())


class CsbIndex():

    def __init__(self, file_pattern):
        self._meta = {}
        fnames = glob.glob(file_pattern, recursive=True)
        for fn in fnames:
            with open(fn, mode='rb') as f:
                self._meta[fn] = CsbReader(f).metadata

    def list_captures(self, **kwargs):
        ret = {}
        for sesf, data in self._meta.items():
            sid, host, addr, iface, captures = itemgetter(
                's', 'h', 'a', 'i', 'c')(data)
            caps = [c for c in captures if _match_filt(kwargs, c)]
            if sesf not in ret:
                ret[sesf] = {'sid': sid, 'host': host,
                             'addr': addr, 'iface': iface, 'captures': caps}

        return ret
