import json
from typing import IO, Iterator
from tarfile import open as tar_open, TarInfo, TarFile
from zstandard import ZstdDecompressor
from .capture import CsbCaptureIterator
from .consts import CSB_ANALYSIS_FILENAME, CSB_METADATA_FILENAME


class CsbReader:

    _tar: TarFile
    _tar_iter: Iterator[TarInfo]

    metadata: dict
    analysis_data: dict

    def __init__(self, source: IO[bytes]):
        zd = ZstdDecompressor()
        zr = zd.stream_reader(source)

        self._tar = tar_open(fileobj=zr, mode='r|')
        self._tar_iter = iter(self._tar)

        meta_tarinfo = next(self._tar_iter)
        assert meta_tarinfo.name == CSB_METADATA_FILENAME
        self.metadata = json.loads(self._extract_data(meta_tarinfo).read())

        anal_tarinfo = next(self._tar_iter)
        assert anal_tarinfo.name == CSB_ANALYSIS_FILENAME
        self.analysis_data = json.loads(self._extract_data(anal_tarinfo).read())

    def __iter__(self):
        return map(self._process_capture, self._tar_iter)

    def _extract_data(self, tarinfo: TarInfo) -> IO[bytes]:
        d = self._tar.extractfile(tarinfo)
        assert d is not None
        return d

    def _process_capture(self, tarinfo: TarInfo):
        stream = self._extract_data(tarinfo)
        return CsbCaptureIterator(tarinfo.name, tarinfo.size, stream)
