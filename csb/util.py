def pack_ip(s):
    a, b, c, d = s.split('.')
    return int(a) << 24 | (int(b) & 0xff) << 16 | (int(c) & 0xff) << 8 | int(d) & 0xff

def unpack_ip(n):
    return f'{n >> 24}.{n >> 16 & 0xff}.{n >> 8 & 0xff}.{n & 0xff}'
