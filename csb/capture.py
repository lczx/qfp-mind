from typing import IO
from .packet_metadata import PacketMetadata


class CsbCaptureIterator:

    name: str
    count: int
    stream: IO[bytes]

    def __init__(self, name, size, stream: IO[bytes]):
        self.name = name
        if size % PacketMetadata.BINARY_LENGTH != 0:
            raise ValueError(f'Capture size must be multiple of {PacketMetadata.BINARY_LENGTH}')
        self.count = size // PacketMetadata.BINARY_LENGTH
        self.stream = stream

    def __iter__(self):
        while True:
            a = self.stream.read(PacketMetadata.BINARY_LENGTH)
            if len(a) == 0:
                break
            yield PacketMetadata(a)

    def __str__(self):
        return f'CsbCaptureIterator({self.name})'
