import struct
from .util import pack_ip, unpack_ip


_STRUCT_FMT = '>dBBHHIIHHdd'

class PacketMetadata:

    BINARY_LENGTH = 42

    def __init__(self, data: bytes):
        if len(data) != PacketMetadata.BINARY_LENGTH:
            raise ValueError('Packet data is not multiple of {PacketMetadata.BINARY_LENGTH} bytes')
        self._d = struct.unpack(_STRUCT_FMT, data)

    def to_bytes(self):
        return struct.pack(_STRUCT_FMT, *self._d)

    @property
    def timestamp(self):
        return self._d[0]

    @property
    def udp(self):
        return self._d[1] >> 4

    @property
    def tcp_alert(self):
        return self._d[1] & 0x0F

    @property
    def tcp_flags(self):
        return self._d[2]

    @property
    def size(self):
        return self._d[3]

    @property
    def conversation(self):
        return self._d[4]

    @property
    def src_ip_raw(self):
        return self._d[5]

    @property
    def src_ip(self):
        return unpack_ip(self.src_ip_raw)

    @property
    def dst_ip_raw(self):
        return self._d[6]

    @property
    def dst_ip(self):
        return unpack_ip(self.dst_ip_raw)

    @property
    def src_port(self):
        return self._d[7]

    @property
    def dst_port(self):
        return self._d[8]

    @property
    def time_relative(self):
        return self._d[9]

    @property
    def time_delta(self):
        return self._d[10]

    def __str__(self):
        return 'PacketMetadata' + str(self._d)
