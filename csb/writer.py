import json
from io import BytesIO
from tarfile import TarInfo, open as tar_open, TarFile
from zstandard import ZstdCompressor
from .consts import CSB_ANALYSIS_FILENAME, CSB_METADATA_FILENAME


class CsbWriter:

    _bio: BytesIO
    _tar: TarFile

    def __init__(self, metadata, analysis):
        self._bio = BytesIO()
        self._tar = tar_open(fileobj=self._bio, mode='w|')

        meta_bytes = bytes(json.dumps(metadata, separators=(',', ':')), 'utf8')
        self.add_data(CSB_METADATA_FILENAME, meta_bytes)
        meta_bytes = bytes(json.dumps(analysis, separators=(',', ':')), 'utf8')
        self.add_data(CSB_ANALYSIS_FILENAME, meta_bytes)

    def write(self):
        self._tar.close()
        zc = ZstdCompressor(level=9)
        return zc.compress(self._bio.getvalue())

    def add_data(self, name, b):
        ti = TarInfo(name)
        ti.size = len(b)
        self._tar.addfile(ti, fileobj=BytesIO(b))

